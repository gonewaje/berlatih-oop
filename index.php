<?php
	require 'animal.php';
	require 'frog.php';
	require 'ape.php';

	$sheep = new Animal("shaun");

	echo $sheep->name; // "shaun"
	echo "<br>";
	echo $sheep->legs; // 2
	echo "<br>";
	echo $sheep->cold_blooded; // false
	echo "<br>";

	// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
	// index.php
	$sungokong = new Ape("kera sakti");
	echo "<br>";
	echo $sungokong->name;
	echo "<br>";
	echo $sungokong->legs;
	echo "<br>";
	$sungokong->yell(); // "Auooo"
	echo "<br>";
	echo $sungokong->cold_blooded;
	echo "<br>";
	echo "<br>";
	$kodok = new Frog("buduk");
	echo "<br>";
	echo $kodok->name;
	echo "<br>";
	echo $kodok->legs;
	$kodok->jump() ; // "hop hop"
	echo "<br>";
	echo $kodok->cold_blooded;
?>